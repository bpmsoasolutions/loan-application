package com.bss.loanapplication.config;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class AbstractRestService {
    protected static Context getInitialContext() throws NamingException {
        return new InitialContext();
    }
}
