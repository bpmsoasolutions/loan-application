package com.bss.loanapplication.config;


import jersey.repackaged.com.google.common.collect.Sets;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;


public class Application extends ResourceConfig{
    public Application() {
        super(Sets.<Class<?>>newHashSet( 
            // resources, other features and providers would also go here
            ResourceFilterBindingFeature.class
        ));
        
        // Specifing packages...
        
        packages("com.bss.loanapplication.rest")
        .register(MultiPartFeature.class)
        .register(JacksonFeature.class);
        
    }
}
