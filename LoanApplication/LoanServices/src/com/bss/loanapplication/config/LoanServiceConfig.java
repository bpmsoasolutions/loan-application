package com.bss.loanapplication.config;

import java.io.FileInputStream;
import java.util.Properties;
import java.io.IOException;
import java.io.InputStream;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import javax.naming.InitialContext;
import javax.naming.NamingException;


import weblogic.rjvm.JVMID;

public class LoanServiceConfig {
    
    private static boolean dataLoaded = false;  

    public static  String PROCESS_MAIN_WSDL = "http://soa-server:7003/soa-infra/services/default/LoanApplication/LoanProcess.service?WSDL";
    public static final String PROCESS_MAIN_QNAME_URI = "http://xmlns.oracle.com/bpmn/bpmnProcess/LoanProcess";
    public static final String PROCESS_MAIN_QNAME_LOCAL =   "LoanProcess.service";    
  
    //private static final String PROCESS_MAIN_PROP_NAME = "config.services.process.proxy.wsdl";

    private static String HTTP = "http";
    
    public static boolean loadConfiguration()  throws IOException {
        
        if(!dataLoaded){
            try{
                  System.out.println("Loading from getDomain(): ");
                  
                  dataLoaded = true;
                  String domain = getDomain(false, HTTP);
                  
                  PROCESS_MAIN_WSDL = domain + "/soa-infra/services/default/LoanApplication/LoanProcess.service?WSDL";
                  
                  System.out.println("PROCESS_MAIN_WSDL EAPPServiceConfig: " + PROCESS_MAIN_WSDL);
                
            } catch (Exception e){
                System.out.println("Excepcion loadConfiguration: " + e.getMessage());
            }
        }
        
        return dataLoaded;
    }
  
    /**
     * Author: MAV
     * Date: 07/04/2016
     *
     * @return
     */
    private static String getDomain(boolean checkSafePort, String protocol)  {
    
        JVMID jvmid = JVMID.localID();
        String serverName = jvmid.getServerName();
        String serverAddress = jvmid.getAddress();
        
        String frontEndAddress = "";
        
        InitialContext ctx;
        boolean https = false;
        int port = 7003;
        
        try {
            ctx = new InitialContext();

            // weblogic.management.configuration.ServerMBean
            MBeanServer server = (MBeanServer) ctx.lookup("java:comp/env/jmx/runtime");
    
            ObjectName objName = new ObjectName("com.bea:Name=" + serverName + ",Type=Server"); 

            port = (Integer) server.getAttribute(objName, "ListenPort");
            
            
            ObjectName objWebServerName = new ObjectName("com.bea:Name=" + serverName + ",Type=WebServer,Server=soa_server1"); 
            
            frontEndAddress = (String) server.getAttribute(objWebServerName, "FrontendHost");
            
            // weblogic.management.configuration.SSLMBean
            ObjectName sslObjName = new ObjectName("com.bea:Name=" + serverName + ",Type=SSL,Server=soa_server1"); 
            
            if(checkSafePort)
                https = (Boolean) server.getAttribute(sslObjName, "Enabled");
            else
                https = false;
            
            if(https && frontEndAddress!= null && !frontEndAddress.equals("")){
                port = (Integer) server.getAttribute(sslObjName, "ListenPort");
            }
            else{
                https = false;
                frontEndAddress = serverAddress;
            }
            
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (MalformedObjectNameException e) {
            e.printStackTrace();
        } catch (ReflectionException e) {
            e.printStackTrace();
        } catch (InstanceNotFoundException e) {
            e.printStackTrace();
        } catch (AttributeNotFoundException e) {
            e.printStackTrace();
        } catch (MBeanException e) {
            e.printStackTrace();
        }
        
        String domainAddress = protocol + (https ? "s" : "") + "://" + frontEndAddress + ":" + port;
        /******
          *System.out.println("\n**************************************");
          *System.out.println("Domain Address EAPPServiceConfig: " + domainAddress);
          *System.out.println("**************************************\n");
        ******/
        
        return domainAddress;
    }

}

