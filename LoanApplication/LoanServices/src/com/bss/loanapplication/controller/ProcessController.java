package com.bss.loanapplication.controller;


import com.bss.loanapplication.config.LoanServiceConfig;

import com.bss.loanapplication.proxy.LoanProcessPortType;

import com.bss.loanapplication.proxy.LoanProcessService;

import com.bss.loanapplication.proxy.types.FormType;

import java.net.URL;

import javax.xml.namespace.QName;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.core.Context;


public class ProcessController {
    
    private static LoanProcessService processMain_ep;
    private static LoanProcessPortType processMain;
    
    public ProcessController()
    throws IOException {
        super();
        
      // Services
        try {
            LoanServiceConfig.loadConfiguration();
        } catch(Exception e) {
            Exception ex = new Exception("Problem loading configuration file in ProcessController.java. Don't care about this, default configuration will be loaded.");
            ex.printStackTrace();
        }
        
        try {
            processMain_ep = new LoanProcessService(new URL(LoanServiceConfig.PROCESS_MAIN_WSDL),
                  new QName(LoanServiceConfig.PROCESS_MAIN_QNAME_URI, LoanServiceConfig.PROCESS_MAIN_QNAME_LOCAL));
            processMain = processMain_ep.getLoanProcessPort();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void createLoan(FormType loanForm, String creator) {
        
        processMain.start(loanForm, creator);
    }
    

    /*
    public static void main(String[] args) throws IOException {
        ProcessMainController kernel = new ProcessMainController();
        kernel.createNewRequest(1, "","","");
        System.out.println("Passed!");
    }
    */
}


