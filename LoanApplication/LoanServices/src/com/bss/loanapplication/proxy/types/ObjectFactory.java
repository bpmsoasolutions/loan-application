
package com.bss.loanapplication.proxy.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bss.loanapplication.proxy.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LoanForm_QNAME = new QName("http://xmlns.bpmsoasolutions.com/loanapplication/config", "loanForm");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bss.loanapplication.proxy.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Start }
     * 
     */
    public Start createStart() {
        return new Start();
    }

    /**
     * Create an instance of {@link FormType }
     * 
     */
    public FormType createFormType() {
        return new FormType();
    }

    /**
     * Create an instance of {@link EndResponse }
     * 
     */
    public EndResponse createEndResponse() {
        return new EndResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.bpmsoasolutions.com/loanapplication/config", name = "loanForm")
    public JAXBElement<FormType> createLoanForm(FormType value) {
        return new JAXBElement<FormType>(_LoanForm_QNAME, FormType.class, null, value);
    }

}
