package com.bss.loanapplication.rest;

import com.bss.loanapplication.config.AbstractRestService;

import com.bss.loanapplication.controller.ProcessController;

import com.bss.loanapplication.proxy.types.FormType;

import com.bss.security.JWTokens;

import com.nimbusds.jwt.ReadOnlyJWTClaimsSet;

import javax.naming.NamingException;
import javax.naming.Context;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/loanProcess")
public class ProcessService extends AbstractRestService {
    
    private static ProcessController processController;

   
    public ProcessService() {
        super();
    
        Context context;
        try {
            context = getInitialContext();

        } catch (NamingException e) {
            throw new RuntimeException(e.getMessage());
            
        }
        
        
        try {
            processController = new ProcessController();
            
        } catch (Exception e) {
            System.out.println(e.getMessage());     
            System.out.println("<REST Service> Impossible to create a instance of ProcessMainController:\n");
            e.printStackTrace();
        } 
        
    }
    
    
    @POST
    @Path("/newLoan")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createNewRequest (FormType loanForm, @javax.ws.rs.core.Context HttpServletRequest request) {
        
        
        String token = getToken(request);
        
        ReadOnlyJWTClaimsSet jw = JWTokens.parseToken(token);
        // Get the subject
        String creator =  (String) jw.getClaim("sub");
        
        System.out.println("Creator: " + creator);
        if (creator == null){
            creator = "jcooper";
        }
        
        try{
            processController.createLoan(loanForm, creator);
        } catch (Exception e) {        
            return Response.status(400).entity("Error al crear la instancia del proceso.").build();
        }
        
        return Response.status(200).entity("Proceso creado correctamente.").build();
    }
    
    private String getToken(HttpServletRequest request) {
        
        String token = (String) request.getHeader("Authorization");
        
        if (token == null) {
            return null;
        }
        
        String[] tokensplit = token.split(" ");
        if (tokensplit[0].equalsIgnoreCase("Bearer")) {
          token = tokensplit[1];
        } else {
          token = tokensplit[0];
        }
        return token;
    }
    
    /*
    @POST
    @Path("/createNewRequest/{appId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createNewRequest (RequestCreate reqCreate, @PathParam("appId")String appId) {
        if(logger.isDebugEnabled()){
            logMsg.setLength(0);                                                // Reset Log Messages
            String rq = reqCreate == null ? "null" : reqCreate.toString();      // Check null pointer
            logger.debug(logMsg.append("<REST Service> createNewRequest Input Parameters:\n").append(rq).append("\n").toString());
        }
        
        long start = System.currentTimeMillis();

        RequestType request = new RequestType();

        request.setItemId(new Long(reqCreate.getItemId()));                     // Set itemId
        request.setFlowId(new  Long(reqCreate.getFlowId()));                    // Set flowId
        request.setOrganizationalUnit(reqCreate.getOU());                       // Set OU
        request.setCreator(reqCreate.getCreator());                             // Set creator
        request.setRequester(reqCreate.getRequester());                         // Set requester
        request.setDescription(reqCreate.getDescription());                     // Set description
        request.setCreatedByAssistant(reqCreate.isCreatedByAssistant());        // Set createdByAssistant     
        request.setIsDraft(reqCreate.isIsDraft());                              // Set isDraft
        request.setRequesterRole(ConstantsEapp.REQUESTER_ROLE);                 // Set requesterRole
        
        FormComponentType createForm = reqCreate.getFormCreation();
        
        if(createForm.getCategories() != null){                                 // If there are Categories
            List<FieldsType> lstFieldType = new ArrayList<FieldsType>();        
            //Go over the categories of the forms
            for(CategoryComponentType category : createForm.getCategories()){
                if(category.getField() != null) {
                    //Go over the fields of each categorie
                    for(FieldComponentType field : category.getField()){        // Go over the Fields of the forms
                        FieldsType fieldT = new FieldsType();
                        fieldT.setName(field.getFieldName());
                        fieldT.setValue(field.getRuntimeConfig().getValue());
                        fieldT.setIsParameter(true);
                        
                        lstFieldType.add(fieldT);                               // Add Field to List
                    }             
                }
                request.setFields(lstFieldType);                                // Add List of Fields to Re
            }
        }
        
        RequestInsertResponse requestInsert;
        // Try insert Request
        try {
                                                           
            String rq = "Null";
            if(request != null){
                rq = request.toString();
            }
            //Creation of the request
            if(logger.isDebugEnabled()){
                logMsg.setLength(0); // Reset Log Messages
                logger.debug(logMsg.append("<REST Service> createNewRequest -->  Calling sessionRequestBean.serviceRequestInsert\nInput Params:\nRequest = ")
                                  .append(rq).append("\n").toString());
            }
            requestInsert = sessionRequestBean.serviceRequestInsert(request);
            
        } catch (Exception e) {                                                 // Exception Insert the Request
            logMsg.setLength(0);                                                // Reset Log Messages
            logMsg.append("<REST Service> createNewRequest --> An error occurred calling sessionRequestBean.serviceRequestInsert.")
                    .append("\nCause: ").append(e.getMessage());
            logger.error(logMsg.toString());          
            return GeneralUtils.respond(500, logMsg.toString());                // Return 500 and Error Message
        }
        
        if(requestInsert.isSuccess()){                                          // Insert Request went Well
            // Map Attachmets
            List<Attachment> attachmentList = new ArrayList<Attachment>();
            if(createForm.getAttachments() != null) {
                Attachment requestAttach;
                Timestamp dateUploaded = new Timestamp(new java.util.Date().getTime());     // Get the Time
                // Get over AttachmentType List        
                for (AttachmentType attachmentType : createForm.getAttachments().getAttachmentInfo()) {
                    requestAttach = new Attachment();                               // Create an empty Attachment Entity
                    requestAttach.setRequestId(requestInsert.getOutput().getRequestId());             // Map requestId
                    
                    requestAttach.setField(attachmentType.getField());              // Map field
                    requestAttach.setLocation(attachmentType.getLocation());        // Map location
                    requestAttach.setFileName(attachmentType.getFileName());        // Map fileName
                    requestAttach.setContentType(attachmentType.getContentType());  // Map contentType
                    requestAttach.setVersion(attachmentType.getVersion().longValue());  // Map version
                    requestAttach.setDescription(attachmentType.getDescription());  // Map description
                    requestAttach.setLabel(attachmentType.getLabel());              // Map label
                    requestAttach.setDateUploaded(dateUploaded);                    // Set Date Uploaded
                    requestAttach.setUserCreation(attachmentType.getUploadedBy());
                    // If we leave null Deleted Field, serviceStepFormAttachsUpdate will fail when try get the value of deleted Field
                    requestAttach.setDeleted(0L);                                   // It's a New Attachment
                    attachmentList.add(requestAttach);                              // Add Attachment to List
                }
            }

            if(attachmentList.size() > 0){                                      // There are attachments
                long att = attachmentList == null ? 0 : attachmentList.size();
                boolean result;    
                try{                                                            // Insert attachments
                    
                    if(logger.isDebugEnabled()){
                        logMsg.setLength(0);                                        // Reset Log Messages
                        logMsg.append("<REST Service> createNewRequest -->  Calling sessionAuditAndAttach.serviceStepFormAttachsUpdate\nInput Params:\nNumber of Attachments = ")
                                          .append(att);
                        logger.debug(logMsg.toString());
                    }
                    
                    
                    result = sessionAuditAndAttach.serviceStepFormAttachsUpdate(requestInsert.getOutput().getRequestId(), attachmentList);
                    if (!result) {                                              // There were problems inserting Attachments
                        logMsg.setLength(0);                                        // Reset Log Messages
                        logMsg.append("<REST Service> createNewRequest --> SessionAuditAndAttachBean.serviceStepFormAttachsUpdate returned false.");
                        logger.error(logMsg.toString());
                        return GeneralUtils.respond(500, logMsg.toString());
                    }else{
                        if(logger.isDebugEnabled()){
                            logMsg.setLength(0);                                // Reset Log Messages
                            logMsg.append("<REST Service> createNewRequest --> SessionAuditAndAttachBean.serviceStepFormAttachsUpdate Output:\nsuccess = true");
                            logger.debug(logMsg.toString());
                        }
                    }
                }catch(Exception ex){                                           // Exception Insert Attachments
                    logMsg.setLength(0);                                        // Reset Log Messages
                    logMsg.append("<REST Service> createNewRequest --> An error occurred calling SessionAuditAndAttachBean.serviceStepFormAttachsUpdate.")
                            .append("\nCause: ").append(ex.getMessage());
                    ex.printStackTrace();
                    logger.error(logMsg.toString());
                    return GeneralUtils.respond(500, logMsg.toString());        // Return 500 and Error Message
                }
            }
       
            logMsg.setLength(0);                                        // Reset Log Messages
            logMsg.append("<REST Service> createNewRequest -->  Calling kernelController.createNewRequestWithAttachments\nInput Params:\n");
            logger.info(logMsg.toString());
            
            logger.info("Time inserting Request from REST Service: " +  (System.currentTimeMillis() - start) / 1000 + "ms");
            
            try{
                /********************** Create Instance of the Request**********
                start = System.currentTimeMillis();
                
                boolean success = processController.createNewRequest(requestInsert.getOutput().getRequestId(), reqCreate.getCreator(), reqCreate.getOU(), appId);
                
                logger.info("Time invoking Process: " +  (System.currentTimeMillis() - start) / 1000 + "ms");
                
                if(success)
                    return Response.status(200).entity(requestInsert.getOutput().getRequestId()).build();
                else
                    return GeneralUtils.respond(500, "Imposible to create and instance of a new process");

            }catch(Exception exc){
                logMsg.setLength(0);                                        // Reset Log Messages
                logMsg.append("<REST Service> createNewRequest --> An error occurred calling kernelController.createNewRequestWithAttachments.")
                      .append("\nCause: ").append(exc.getMessage());
                logger.error(logMsg.toString());
                return GeneralUtils.respond(500, logMsg.toString());        // Return 500 and Error Message
            }
        }   
        // There were problems Inserting the Request
        return GeneralUtils.respond(500, "The Request can't be created");
    }
    
    */
}
